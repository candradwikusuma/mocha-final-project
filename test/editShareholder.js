
const chai = require('chai')
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const expect = chai.expect;
const { Random } = require('random-js');
const baseUrl = 'https://be.investree.tech';
const baseUrlShareholder = 'https://svc.investree.tech';
const pathUrlLogin = '/auth/login/frontoffice';
const pathUrlRegistration = '/auth/registration/borrower';
const pathUrlOtp='/frontoffice/otp/verify'
const pathUrlProductPreference='/frontoffice/borrower/product-preference'
let pathUrlCreateShareholder='/validate/customer/shareholders-information?userRoleType=1'
const pathUrlUpdateShareholder='/validate/customer/shareholders-information/156192?userRoleType=1'


let requestRegister;
let requestOtp;
let requestProductPreference;
let headerRequest;
let shareholderRequest;
let updateRequestRegister
describe('Update Shareholder',()=>{
   headerRequest ={
    'x-investree-key': '7574a0e42176f3766d2429fc77b96a411ec75e7700ed7407f48489e6889f4d76',
    'x-investree-signature': 'df8aa07026f5a5198ee0e52c217f5bdc8881035f2c1e3fc39994947899d1894c',
    'x-investree-timestamp': '2022-01-24T16:48:01',
    'x-investree-token': null
   }

   beforeEach(async ()=>{
    shareholderRequest={
        "userRoleType": 1,
        "customerId": null,
        "position": 2,
        "fullName": randomAlphabet(),
        "mobilePrefix": 2,
        "mobileNumber": randomNumberHP(),
        "emailAddress": `test-${randomNumber()}@gmail.com`,
        "stockOwnership": 1,
        "dob": "1997-03-03",
        "identificationCardUrl": "https://oss.investree.tech/NPWP_NOMOR_POKOK_WAJIB_PAJAK/NPWP_NOMOR_POKOK_WAJIB_PAJAK_G8dtKq_1643246862572_.jpg",
        "identificationCardNumber": randomNumberKTP(),
        "identificationCardExpiryDate": "3000-12-31",
        "idCardExpiryLifetime": true,
        "selfieUrl": "https://oss.investree.tech/KTP_SELFIE/KTP_SELFIE_yBtAbr_1643257023066_.jpg",
        "taxCardUrl": "https://oss.investree.tech/NPWP_NOMOR_POKOK_WAJIB_PAJAK/NPWP_NOMOR_POKOK_WAJIB_PAJAK_G8dtKq_1643246862572_.jpg",
        "taxCardNumber": randomNumberNPWP(),
        "shareHolderId": ""
    }
       updateRequestRegister={
        "userRoleType": 1,
            "customerId": 274610,
            "position": 2,
            "fullName": randomAlphabet(),
            "mobilePrefix": 2,
            "mobileNumber": randomNumberHP(),
            "emailAddress": `test-${randomNumber()}@gmail.com`,
            "stockOwnership": 1,
            "dob": "1997-03-03",
            "identificationCardUrl": "https://oss.investree.tech/NPWP_NOMOR_POKOK_WAJIB_PAJAK/NPWP_NOMOR_POKOK_WAJIB_PAJAK_G8dtKq_1643246862572_.jpg",
            "identificationCardNumber": randomNumberKTP(),
            "identificationCardExpiryDate": "3000-12-31",
            "idCardExpiryLifetime": true,
            "selfieUrl": "https://oss.investree.tech/KTP_SELFIE/KTP_SELFIE_yBtAbr_1643257023066_.jpg",
            "taxCardUrl": "https://oss.investree.tech/NPWP_NOMOR_POKOK_WAJIB_PAJAK/NPWP_NOMOR_POKOK_WAJIB_PAJAK_G8dtKq_1643246862572_.jpg",
            "taxCardNumber": randomNumberNPWP(),
            "shareHolderId": null,
            "dateOfBirth": "1993-11-03",
            "id": null
    }
    
    requestProductPreference={
        "userCategory": 2,
        "productPreference": 3,
        "productSelection": 2,
        "legalEntity": 1,
        "companyName": "Anak Ayam"
    }
    requestRegister={
        "userType": 1,
        "salutation": "Mr.",
        "fullname": `Fullname ${randomAlphabet()}`,
        "nationality": 104,
        "email": `test-${randomNumber()}@gmail.com`,
        "mobilePrefix": 1,
        "phoneNumber": randomNumber(),
        "username": `username${randomNumber()}`,
        "password": "Candradk7",
        "referralCode": "",
        "agreePrivacy": true,
        "agreeSubscribe": true,
        "captcha": "qa-bypass-captcha"
    }
    //register
        const responseRegister = await chai
            .request(baseUrl)
            .post(pathUrlRegistration)
            .set(headerRequest)
            .send(requestRegister);
        expect(responseRegister.status,'Error Registrasi borrower').to.equal(200)//guard statement
        headerRequest['x-investree-token']=responseRegister.body.data.accessToken
        shareholderRequest['customerId']=responseRegister.body.data.customerId
        console.log('Register borrower with valid credential should success');
      
    //otp
        const responseOtp=await chai
            .request(baseUrl)
            .post(pathUrlOtp)
            .set(headerRequest)
            .send({
                "otp": "123456",
                "isVerifyOtpPartner": false
            });
            // console.log(responseOtp);
            // expect(responseOtp)
            expect(responseOtp.status,'Error OTP borrower').to.equal(200)//guard statement
            console.log('Otp borrower with valid credential should success');
        //productpreference
            const responseProductPreference=await chai
            .request(baseUrl)
            .post(pathUrlProductPreference)
            .set(headerRequest)
            .send(requestProductPreference)
            expect(responseProductPreference.status,'Product Preference  borrower').to.equal(200)//guard statement
            console.log('Product Preference should success');

         //login
            const responseLogin= await chai
            .request(baseUrl)
            .post(pathUrlLogin)
            .set(headerRequest)
            .send({
                "email": requestRegister.email,//jangan di hardcorde
                "password": requestRegister.password,
                "captcha": requestRegister.captcha
            })

            expect(responseLogin.status,'Gagal Login').to.equal(200)
            console.log('Login borrower with valid credential should success');
        //create 
        // console.log(shareholderRequest);
        const responseCreate= await chai
            .request(baseUrlShareholder)
            .post(pathUrlCreateShareholder)
            .set(headerRequest)
            .send(shareholderRequest)
        //    console.log(shareholderRequest);
        //    console.log(responseCreate.body);
            expect(responseCreate.status,'Gagal Create').to.equal(200)
            updateRequestRegister['shareHolderId']=responseCreate.body.data.id
            updateRequestRegister['id']=responseCreate.body.data.id
            // console.log( headerRequest['identificationCardNumber']);
            console.log('Create shareholder with valid credential should success');
        
   })
 
    it('Update shareholder with valid credential should success',async()=>{
        shareholderRequest['fullName']='CAnDRA DWI KUSUMA'
        // shareholderRequest.identificationCardNumber = randomNumber(16)
        const responseUpdate= await chai
            .request(baseUrlShareholder)
            .put(pathUrlUpdateShareholder)
            .set(headerRequest)
            .send(updateRequestRegister);
            // console.log(updateRequestRegister);
            console.log(responseUpdate.status);
            expect(responseUpdate.status,'Gagal Update').to.equal(200)
           
           
    })
    it('Update shareholder with invalid credential should failed',async()=>{
       shareholderRequest['identificationCardNumber']=17
        const responseUpdate= await chai
            .request(baseUrlShareholder)
            .put(pathUrlUpdateShareholder)
            .set(headerRequest)
            .send(shareholderRequest)
    //   console.log(responseUpdate.body);
            expect(responseUpdate.status,'Gagal Create').to.equal(400)
    })
  
 
    
})

function randomNumber(length = 9) {
    const pool = '1234567890';

    let result = new Random().string(length, pool);
    if (result.charAt(0) === '0') {
        const randomNum = new Random().integer(1, 9);
        result = randomNum + result.slice(1, result.length);
    }

    return result;
}
function randomNumberHP(length = 12) {
    const pool = '1234567890';

    let result = new Random().string(length, pool);
    if (result.charAt(0) === '0') {
        const randomNum = new Random().integer(1, 9);
        result = randomNum + result.slice(1, result.length);
    }

    return result;
}
function randomNumberKTP(length = 16) {
    const pool = '1234567890';

    let result = new Random().string(length, pool);
    if (result.charAt(0) === '0') {
        const randomNum = new Random().integer(1, 9);
        result = randomNum + result.slice(1, result.length);
    }

    return result;
}
function randomNumberNPWP(length = 15) {
    const pool = '1234567890';

    let result = new Random().string(length, pool);
    if (result.charAt(0) === '0') {
        const randomNum = new Random().integer(1, 9);
        result = randomNum + result.slice(1, result.length);
    }

    return result;
}

function randomAlphabet(length = 12) {
    const pool = 'abcdefghijklmnopqrstuvwxyz';

    let result = new Random().string(length, pool);
    if (result.charAt(0) === '0') {
        const randomAlpha = new Random().string(1, 25);
        result = randomAlpha + result.slice(1, result.length);
    }

    return result;
}