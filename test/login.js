const chai = require('chai')
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const expect = chai.expect;
const { Random } = require('random-js');
const baseUrl = 'https://be.investree.tech';
const pathUrlLogin = '/auth/login/frontoffice';
const pathUrlRegistration = '/auth/registration/borrower';
const pathUrlOtp='/frontoffice/otp/verify'

let requestRegister;
let requestOtp;
let headerRequest;

describe('POST Loggin Borrower',()=>{
   headerRequest ={
    'x-investree-key': '7574a0e42176f3766d2429fc77b96a411ec75e7700ed7407f48489e6889f4d76',
    'x-investree-signature': 'df8aa07026f5a5198ee0e52c217f5bdc8881035f2c1e3fc39994947899d1894c',
    'x-investree-timestamp': '2022-01-24T16:48:01',
    'x-investree-token': null
   }
   before(async ()=>{
    requestOtp={
        "otp": "123456",
        "isVerifyOtpPartner": false
    }
    requestRegister={
        "userType": 1,
        "salutation": "Mr.",
        "fullname": `Fullname ${randomAlphabet()}`,
        "nationality": 104,
        "email": `test-${randomNumber()}@gmail.com`,
        "mobilePrefix": 1,
        "phoneNumber": randomNumber(),
        "username": `username${randomNumber()}`,
        "password": "Candradk7",
        "referralCode": "",
        "agreePrivacy": true,
        "agreeSubscribe": true,
        "captcha": "qa-bypass-captcha"
    }
    
        const response = await chai
            .request(baseUrl)
            .post(pathUrlRegistration)
            .set(headerRequest)
            .send(requestRegister);
        expect(response.status,'Error Registrasi borrower').to.equal(200)//guard statement
        headerRequest['x-investree-token']=response.body.data.accessToken
        console.log('Register borrower with valid credential should success');
//   

        const responseOtp=await chai
            .request(baseUrl)
            .post(pathUrlOtp)
            .set(headerRequest)
            .send(requestOtp);
            expect(responseOtp.status,'Error OTP borrower').to.equal(200)//guard statement
            console.log('Otp borrower with valid credential should success');
   })
 
   

    it('Login borrower with valid credential should success',async ()=>{
        // console.log(requestBody);
        const response= await chai
            .request(baseUrl)
            .post(pathUrlLogin)
            .set(headerRequest)
            .send({
                "email": requestRegister.email,//jangan di hardcorde
                "password": requestRegister.password,
                "captcha": requestRegister.captcha
            })
// console.log(response);
            expect(response.status).to.equal(200)
            //expect(response.body.data.otpVerificationStatus).to.equal(true)

    })

    it('Login borrower with invalid credential should failed',async()=>{
        // console.log(requestBody);
        const response= await chai
            .request(baseUrl)
            .post(pathUrlLogin)
            .set(headerRequest)
            .send( {
                "email":  `${Math.random().toString(36)}@gmail.com`,
                "password": requestRegister.password,
                "captcha": requestRegister.captcha
            })
            expect(response.status).to.equal(400)
            // console.log(response);
    })
})

function randomNumber(length = 9) {
    const pool = '1234567890';

    let result = new Random().string(length, pool);
    if (result.charAt(0) === '0') {
        const randomNum = new Random().integer(1, 9);
        result = randomNum + result.slice(1, result.length);
    }

    return result;
}

function randomAlphabet(length = 12) {
    const pool = 'abcdefghijklmnopqrstuvwxyz';

    let result = new Random().string(length, pool);
    if (result.charAt(0) === '0') {
        const randomAlpha = new Random().string(1, 25);
        result = randomAlpha + result.slice(1, result.length);
    }

    return result;
}